import React, { useState } from "react";
import "../HashtagTool/HashTagTool.css";

import { SimplePool, nip19 } from "nostr-tools";
import ContentCard from "../ContentCard";

const CheckMuted = (props) => {
  const [pubkey, setPubkey] = useState(null);
  const [notes, setNotes] = useState([]);
  const [kind0List, setKind0List] = useState([]);

  const relays = [
    "wss://relay.damus.io/",
    "wss://offchain.pub/",
    "wss://nos.lol/",
    "wss://relay.nostr.wirednet.jp/",
    "wss://nostr.wine/",
  ];

  const handleChange = (event, setter) => {
    setter(event.target.value);
  };

  const inputs = [
    {
      label: "Your Pubkey",
      handler: (event) => {
        handleChange(event, setPubkey);
      },
      variable: pubkey,
    },
  ];

  const handleClick = async () => {
    const relayPool = new SimplePool();
    //console.log("textt", nip19.decode());
    const filters = {
      "#d": ["mute"],
    };
    if (pubkey && pubkey.length !== 0)
      filters["#p"] = [nip19.decode(pubkey).data];
    console.log("filters", filters);
    let notes = await relayPool.list(relays, [filters]);
    console.log("muted", notes);
    setNotes(notes);
    const kind0Filters = {
      kinds: [0],
      authors: notes.map((note) => {
        return note.pubkey;
      }),
    };
    const kind0Result = await relayPool.list(relays, [kind0Filters]);
    setKind0List(kind0Result);
    //setMuteList(muted);
    // console.log("muteList", muteList);
  };

  return (
    <div className="matrix-input-container">
      <div className="inputWrapper">
        {inputs.map((input) => {
          return (
            <div>
              <input
                type="text"
                text="npub..."
                placeholder={input.label}
                onChange={input.handler}
                className="matrix-input"
              />
            </div>
          );
        })}
      </div>
      <button className="play-game-button matrix-style" onClick={handleClick}>
        See Results
      </button>
      <div className="results">
        {notes.map((note) => {
          return (
            <ContentCard
              key={note.id}
              pubKey={note.pubkey}
              content={nip19.npubEncode(note.pubkey)}
              kind0List={kind0List}
            />
          );
        })}
      </div>
    </div>
  );
};

export default CheckMuted;
